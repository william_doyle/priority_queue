#pragma once
#include <stdlib.h>
#include <stdio.h>

// simple node based priority queue for storing void pointers
// william doyle
// Febuary 13th 2020

struct pq_node{
	void * data;
	struct pq_node * next;
};

struct pqueue{
	void*(*dequeue)(struct pqueue *);
	void(*enqueue)(struct pqueue *, void*);
	int (*compare)(void*, void*);
	void (*print_raw)(struct pqueue *);
	struct pq_node * top;
};

void destroy_pqueue(struct pqueue *);
struct pq_node * make_pq_node(void *);
struct pqueue * make_pqueue(int(*)(void*, void*));

void * __dequeue(struct pqueue * );
void __enqueue(struct pqueue * , void *);
void __print_raw(struct pqueue *);
int __int_compare(void *, void *);

int __float_compare(void *, void *) __attribute__((depricated("__float_compare is unstable and should not be used!"))); // don't use -> highly unstable
int __short_compare(void *, void *);
int __long_compare(void *, void *);
int __unsigned_compare(void *, void *);
int __double_compare(void *, void *);
int __string_compare(void *, void *);

