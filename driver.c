#include "pqueue.h"

//#define TEST_INT
#define TEST_FLOAT

int main(void){

#ifdef TEST_INT
	int * n1 = malloc(sizeof(int));
	int * n2 = malloc(sizeof(int));
	int * n3 = malloc(sizeof(int));
	int * n4 = malloc(sizeof(int));
	int * n5 = malloc(sizeof(int));
	int * n6 = malloc(sizeof(int));
	int * n7 = malloc(sizeof(int));
	int * n8 = malloc(sizeof(int));
	*n1 = 2;
	*n2 = 2;
	*n3 = 1;
	*n4 = 0;
	*n5 = 0;
	*n6 = -1;
	*n7 = -2;
	*n8 = -1;
	struct pqueue * pq = make_pqueue(__int_compare);
	pq->enqueue(pq, (void *)n8);
	pq->print_raw(pq);
	printf("\n");
	pq->enqueue(pq, (void *)n3);
	pq->print_raw(pq);
	printf("\n");
	pq->enqueue(pq, (void *)n4);
	pq->print_raw(pq);
	printf("\n");
	pq->enqueue(pq, (void *)n5);
	pq->print_raw(pq);
	printf("\n");
	pq->enqueue(pq, (void *)n6);
	pq->print_raw(pq);
	printf("\n");
	pq->enqueue(pq, (void *)n7);
	pq->print_raw(pq);
	printf("\n");
	pq->enqueue(pq, (void *)n1);
	pq->print_raw(pq);
	printf("\n");
	pq->enqueue(pq, (void *)n2);
	pq->print_raw(pq);
	printf("\n");


	destroy_pqueue(pq);
	free(n1);
	free(n2);
	free(n3);
	free(n4);
	free(n5);
	free(n6);
	free(n7);
	free(n8);
#endif


#ifdef TEST_FLOAT
	float * n1 = malloc(sizeof(float));
	float * n2 = malloc(sizeof(float));
	float * n3 = malloc(sizeof(float));
	float * n4 = malloc(sizeof(float));
	float * n5 = malloc(sizeof(float));
	float * n6 = malloc(sizeof(float));
	float * n7 = malloc(sizeof(float));
	float * n8 = malloc(sizeof(float));
	*n1 = 1.0f;
	*n2 = 2.0f;
	*n3 = 1.012f;
	*n4 = 0.9f;
	*n5 = 0.0f;
	*n6 = -1.0f;
	*n7 = -2.0f;
	*n8 = -1.8f;
	struct pqueue * pq = make_pqueue(__float_compare);
	pq->enqueue(pq, (void *)n2);
	pq->enqueue(pq, (void *)n3);
	pq->enqueue(pq, (void *)n4);
	pq->enqueue(pq, (void *)n5);
	pq->enqueue(pq, (void *)n6);
	pq->enqueue(pq, (void *)n7);
	pq->enqueue(pq, (void *)n8);
	pq->enqueue(pq, (void *)n1);

	printf("TOP: %f\n", *(float*)pq->dequeue(pq));
	printf("TOP: %f\n", *(float*)pq->dequeue(pq));
	printf("TOP: %f\n", *(float*)pq->dequeue(pq));
	printf("TOP: %f\n", *(float*)pq->dequeue(pq));
	printf("TOP: %f\n", *(float*)pq->dequeue(pq));
	printf("TOP: %f\n", *(float*)pq->dequeue(pq));
	printf("TOP: %f\n", *(float*)pq->dequeue(pq));
	printf("TOP: %f\n", *(float*)pq->dequeue(pq));

	destroy_pqueue(pq);
	free(n1);
	free(n2);
	free(n3);
	free(n4);
	free(n5);
	free(n6);
	free(n7);
	free(n8);
#endif

}
